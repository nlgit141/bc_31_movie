import axios from "axios";
import { batLoadingAction, tatLoadingAction } from "../redux/actions/spinnerAction";
import { store } from "../../src/index";

export const TOKEN_CYBER =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCDEkMOgIE7hurVuZyAwMyIsIkhldEhhblN0cmluZyI6IjIwLzAxLzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY3NDE3MjgwMDAwMCIsIm5iZiI6MTY0NTgwODQwMCwiZXhwIjoxNjc0MzIwNDAwfQ.8_aCoaa6rU0qnQpITJH8MZSFEBfvbj11eFJWuFsTYL8";

export let https = axios.create({
  baseURL: "https://movienew.cybersoft.edu.vn",

  headers: {
    TokenCybersoft: TOKEN_CYBER,
  },

});


https.interceptors.request.use(function (config) {
  store.dispatch(batLoadingAction());

  console.log("https.interceptors.request");
  // Do something before request is sent
  return config;
}, function (error) {
  store.dispatch(tatLoadingAction());

  // Do something with request error
  return Promise.reject(error);
});

// Add a response interceptor
https.interceptors.response.use(function (response) {
  store.dispatch(tatLoadingAction());
  // Any status code that lie within the range of 2xx cause this function to trigger
  // Do something with response data
  return response;
}, function (error) {


  // Any status codes that falls outside the range of 2xx cause this function to trigger
  // Do something with response error
  return Promise.reject(error);
});
