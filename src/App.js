import logo from "./logo.svg";
import "./App.css";
import LoginPage from "./Page/LoginPage/LoginPage";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./Page/HomePage/HomePage";
import HeaderTheme from "./components/HeaderTheme/HeaderTheme";
import Layout from "./HOC/Layout";
import DetailPage from "./Page/DetailPage/DetailPage";
import SpinnerComponent from "./components/spinner/SpinnerComponent";

function App() {
  return (
    <div className="">
      <SpinnerComponent />
      <BrowserRouter>
        <Routes>
          {/* <Route path="/" element={<Layout Component={<HomePage />} />} /> */}
          <Route path="/" element={<Layout Component={HomePage} />} />
          <Route path="/detail/:id" element={<Layout Component={DetailPage} />} />
          <Route path="/login" element={<LoginPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
