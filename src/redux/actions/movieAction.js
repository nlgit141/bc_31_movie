import { movieService } from "../../services/movie.service";
import { SET_MOVIE_LIST } from "../contants/movieContants";

export const getMovieListActionService = () => {
    return (distpatch) => {
        movieService.getMovieList().then((res) => {
            console.log(res);
            distpatch({
                type: SET_MOVIE_LIST,
                payload: res.data.content
            });
        })
            .catch((err) => {
                console.log(err);
            });
    };
};