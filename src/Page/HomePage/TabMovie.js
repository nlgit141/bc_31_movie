import React, { useEffect, useState } from 'react';
import { Tabs } from 'antd';
import { movieService } from '../../services/movie.service';
import ItemTabMovie from './ItemTabMovie';
import SpinnerComponent from '../../components/spinner/SpinnerComponent';


const { TabPane } = Tabs;

export default function TabMovie() {
    const [dataMovie, setDataMovie] = useState([]);

    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setIsLoading(true); //! spinner

        movieService
            .getMovieByTheater()
            .then((res) => {
                setIsLoading(false);
                console.log(res);
                setDataMovie(res.data.content);
            })
            .catch((err) => {
                setIsLoading(false);
                console.log(err);
            });
    }, []);


    const onChange = (key) => {
        console.log(key);
    };
    const renderContent = () => {
        return dataMovie.map((heThongRap, index) => {
            return (<TabPane tab={<img className='w-16' src={heThongRap.logo} />} key={index}>
                <Tabs style={{ height: 500, color: "white" }} tabPosition='left' defaultActiveKey="1" onChange={onChange}>
                    {heThongRap.lstCumRap.map((cumRap, index) => {
                        return <TabPane tab={renderTenCumRap(cumRap)} key={cumRap.maCumRap}>
                            <div style={{ height: 500, overflowY: "scroll" }} className="shadow-lg">
                                {cumRap.danhSachPhim.map((phim, index) => {
                                    return <ItemTabMovie phim={phim} key={index} />;
                                })}
                            </div>
                        </TabPane>;
                    })}
                </Tabs>
            </TabPane>);
        });
    };

    const renderTenCumRap = (cumRap) => {
        return <div className='text-left w-60 whitespace-normal'>
            <p className="text-green-500 truncate">{cumRap.tenCumRap}</p>
            <p className="text-white truncate">{cumRap.diaChi}</p>
            <button className='text-red-500'>[Xem chi tiết] {`{ }`}</button>
        </div>;
    };



    return (
        <div className='container mx-auto py-20 pb-96 bg-slate-700' >
            {/* {isLoading ? <SpinnerComponent /> : ()} */}
            <Tabs style={{ height: 500, color: "white" }} tabPosition='left' defaultActiveKey="1" onChange={onChange}>
                {renderContent()}
            </Tabs>
        </div>
    );
}
