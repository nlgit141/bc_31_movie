import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getMovieListActionService } from "../../redux/actions/movieAction";
import { Card } from 'antd';
import { NavLink } from "react-router-dom";
const { Meta } = Card;

export default function ListMovie() {
  let dispatch = useDispatch();
  let { movieList } = useSelector((state) => {
    return state.movieReducer;
  });
  console.log("movieList: ", movieList);
  useEffect(() => {

    // movieService
    //   .getMovieList()
    //   .then((res) => {
    //     console.log(res);
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });

    dispatch(getMovieListActionService());
  }, []);


  let renderMovieList = () => {
    return movieList.filter((item) => {
      return item.hinhAnh !== null;
    })
      .map((item) => {
        return (
          <Card key={item.maPhim}

            hoverable
            style={{ width: "100%" }}
            className="shadow-xl hover:shadow-slate-600 hover:shadow-xl"
            cover={<img alt="example" src={item.hinhAnh} />}
          >
            <Meta title={<p className="text-blue-500">{item.biDanh}</p>} description="www.instagram.com" />
            <NavLink to={`detail/${item.maPhim}`}><button className="w-full bg-red-600 text-white rounded text-xl font-medium py-3">Mua vé</button></NavLink>
          </Card >
        );
      });
  };

  return <div className="grid grid-cols-4 gap-10 container mx-auto px-20 py-10">{renderMovieList()}</div>;
}
