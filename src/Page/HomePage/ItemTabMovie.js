import moment from 'moment';
import React from 'react';


export default function ItemTabMovie({ phim }) {
    return (

        <div className='flex space-x-10 bg-slate-700'>
            <img className='w-24 h-48 object-cover my-2' src={phim.hinhAnh} alt="" />
            <div>
                <p className='font-medium text-2xl text-white mb-5'>{phim.tenPhim}</p>
                <div className='grid grid-cols-4 gap-4'>
                    {phim.lstLichChieuTheoPhim.slice(0, 8).map((lichChieu) => {
                        return <div className='bg-red-700 text-white p-2 rounded'>{moment(lichChieu.ngayChieuGioChieu).format("DD-MM-YYYY")}
                            <span className='text-yellow-400 font-bold text-base ml-5'>
                                {moment(lichChieu.ngayChieuGioChieu).format("hh:mm")}
                            </span>
                        </div>;
                    })}
                </div>
            </div>
        </div>


    );
}
