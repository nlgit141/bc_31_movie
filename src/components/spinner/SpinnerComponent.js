import React from 'react';
import { useSelector } from 'react-redux';
import { PacmanLoader } from 'react-spinners';

export default function SpinnerComponent() {

    let { isLoading } = useSelector((state) => {
        return state.spinnerReducer;
    });

    return isLoading ? (
        <div style={{ backgroundColor: "#f90000" }} className='h-screen w-screen fixed top-0 left-0 overflow-hidden items-center justify-center flex z-50 ' >
            <PacmanLoader size={100} />
        </div >
    ) : ("");
}
